// Tableau 2D.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#define NB_MOT_DICO 20
#define SIZE_MAX_MOT 20
struct Dico
{
	char m_Tab_word[NB_MOT_DICO][SIZE_MAX_MOT];
	int Nb_Mot = 0;
};
void Get_Entry(char New_Mots[])
{
	printf("\n Vous pouvez entrez un mot ");
	scanf_s("%s", New_Mots, sizeof(char)*SIZE_MAX_MOT);
}
int Cmp_Word(char Word_1[], char Word_2[])
{
	int i = 0;
	while (Word_1[i] != '\0' && Word_2[i] != '\0')
	{
		if(Word_1[i] > Word_2[i])
		{
			return 1;
		}
		if (Word_1[i] < Word_2[i])
		{
			return -1;
		}
		i++;
	}
	if (Word_1[i] != '\0')
	{
		return 1;
	}
	else if (Word_2[i] != '\0')
	{
		return -1;
	}
	return 0;
}
int Find_Place(Dico _Dico, char New_mot[])
{
	int Retour = 0;
	for (int i = 0; i < _Dico.Nb_Mot; i++)
	{
		
		int Cmp = Cmp_Word(_Dico.m_Tab_word[i], New_mot);
		if (Cmp == 1)
		{
			return i;
		}
		if (Cmp == 0)
		{
			return -1;
		}
		
	}
	return _Dico.Nb_Mot;
}
void Swap_Word(char Word_1[], char Word_2[])
{
	int IsNot_Null = 1;
	int i = 0;
	char temp = '\0';
	while ((Word_1[i] != '0' || Word_2[i] != '\0'))
	{
		temp = Word_1[i];
		Word_1[i] = Word_2[i];
		Word_2[i] = temp;
		if (Word_1[i] == '\0' && Word_2[i] == '\0')
		{
			return;
		}
		i++;
	}
}
void Insert_Tab(Dico &_Dico, char New_Mot[], int Index)
{
	
	for (int i = _Dico.Nb_Mot; i > Index; i--)
	{
		Swap_Word(_Dico.m_Tab_word[i-1],_Dico.m_Tab_word[i]);
	}


	Swap_Word(_Dico.m_Tab_word[Index],New_Mot);
	_Dico.Nb_Mot++;
}
void Suppr_Elem(Dico &_Dico)
{
	int Index = 0;
	char Delete[SIZE_MAX_MOT] = {'\0'};
	printf("\nA quelle position voules vous supprimer l'élément");
	scanf_s("%d", &Index);
	Swap_Word(_Dico.m_Tab_word[Index], Delete);
	for (int i = Index; i < _Dico.Nb_Mot; i++)
	{
		Swap_Word(_Dico.m_Tab_word[i], _Dico.m_Tab_word[i+1]);
	}
	_Dico.Nb_Mot--;
}
void Print_Tab(Dico _Dico)
{
	for (int i = 0; i < _Dico.Nb_Mot; i++)
	{
		printf("%s\n",_Dico.m_Tab_word[i],sizeof(_Dico.m_Tab_word[i]));
	}
}
void User_Add_Word(Dico &_Dico)
{
	char New_Mot[SIZE_MAX_MOT];
	int Place_Word = 0;
	Get_Entry(New_Mot);
	Place_Word = Find_Place(_Dico, New_Mot);
	Insert_Tab(_Dico, New_Mot, Place_Word);
}
void Init_Dico(Dico& _Dico)
{
	for (int i = 0; i < NB_MOT_DICO;i++)
	{
		for (int j = 0; j < SIZE_MAX_MOT; j++)
		{
			_Dico.m_Tab_word[i][j] = '\0';
		}
	}
}
int main()
{
    std::cout << "Hello World!\n";
	Dico _Dico;
	_Dico.Nb_Mot = 0;
	Init_Dico(_Dico);
	int Choice = NULL;
	while (Choice != 4)
	{		
		printf("1: Ajouter un mot au tableau \n 2: Supprimer un Mot \n 3: Afficher le tableau \n 4: Quitter");
		scanf_s("%d", &Choice);
		switch (Choice)
		{
		case 1: 
			User_Add_Word(_Dico);
			break;

		case 2:
			Suppr_Elem(_Dico);
			break;

		case 3:
			Print_Tab(_Dico);
			break;

		default:
			break;
		}
	}
}